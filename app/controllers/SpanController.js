/**
 * Created by MBM info on 15/05/2016.
 */
(function () {

    var SpanController = function ($location,$timeout,$scope, InfoService) {
        /**
         * Init function allow to init data into $scope
         * By defaults Values
         *
         */

        $scope.title = '';
        $scope.showInfo = function (info) {
            InfoService.setInfo(info);
            $location.path('/detail')

        };
        $scope.infos = [];

        function init() {

            InfoService.getSpanLeague().success(function (data) {
                $scope.infos = data.responseData.feed.entries;
                $scope.title = data.responseData.feed.title;
                $scope.$apply();

            });
        };
        /**
         * Rafrichir data
         */
        function refresh(){

            setInterval(function(){
                InfoService.getSpanLeague().success(function (data) {
                    $scope.infos = data.responseData.feed.entries;
                    $scope.$apply();

                });

            },3000);
        }

        refresh();
        init();


    };
    SpanController.$inject = ['$location','$timeout','$scope', 'InfoService'];

    angular.module("LEAGUE").controller("SpanController", SpanController);


})();
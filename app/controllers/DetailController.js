/**
 * Created by MBM info on 04/04/2015.
 */
(function () {

    var DetailController = function ($scope, InfoService) {

        $scope.currentInfo = null;
        function init() {
            $scope.currentInfo = InfoService.detail;

        }

        init();

    };
    DetailController.$inject = ['$scope', 'InfoService'];
    angular.module("LEAGUE").controller("DetailController", DetailController);


})();
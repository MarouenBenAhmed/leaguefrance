/**
 * Created by MBM info on 15/05/2016.
 */
(function () {

    var InfoController = function ($location,$scope, InfoService) {
        /**
         * Init function allow to init data into $scope
         * By defaults Values
         *
         */


        $scope.showInfo = function (info) {
           InfoService.setInfo(info);
            $location.path('/detail')

        };
        $scope.infos = [];

        function init() {

            InfoService.getInfos().success(function (data) {
                $scope.infos = data.responseData.feed.entries;
                $scope.$apply();

            });
        };
        function refresh(){

            setInterval(function(){
                InfoService.getInfos().success(function (data) {
                    $scope.infos = data.responseData.feed.entries;
                    $scope.$apply();

                });

            },3000);
        }

        refresh();


        init();


    };
    InfoController.$inject = ['$location','$scope', 'InfoService'];

    angular.module("LEAGUE").controller("InfoController", InfoController);


})();
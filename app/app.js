/**
 * This file contains general config and routes of our SPA HR DATA APPLICATION
 *
 *
 */
(function(){
    var app=angular.module("LEAGUE",['ngRoute']);
    /**
     * app.config make us able to config out routing using the given module By ANGULAR JS
     * NAMED ngRoute
     *
     *
     */
    app.config(['$routeProvider',
        function($routeProvider) {
            $routeProvider.when(
                '/',{
                    templateUrl: 'app/views/info.html',
                    controller: 'InfoController'

                }).when(
                '/detail',{
                    templateUrl: 'app/views/detail.html',
                    controller: 'DetailController'

                }).when(
                '/FRENCH',{
                    templateUrl: 'app/views/france.html',
                    controller: 'FranceController'}).when(
                '/SPAGNE',{
                    templateUrl: 'app/views/spangne.html',
                    controller: 'SpanController'})
                .otherwise({
                    redirectTo: '/'
                });


        }]);

/**
 * app.run while listen to the event linked to
 * load of our Angular js App
 **/

    /**
     * app.run while listen to the event linked to
     * load of our Angular js App
     **/
    app.run(function($rootScope,$location,InfoService) {
        $rootScope.$on( "$routeChangeStart", function(event, next, current) {

            if($location.$$path == '/detail'){
                if(InfoService.detail == null)
                $location.path('/')
            }

        });
    });



})();

/**
 * Created by MBM info on 05/04/2015.
 */
(function () {
    var InfoService = function ($location, RssService) {
        var rssInfoUrl = 'http://www.matchendirect.fr/rss/info.xml';
        var rssFrenshUrl = 'http://www.matchendirect.fr/rss/foot-ligue-1-c16.xml'
        var rssSpanUrl ='http://www.matchendirect.fr/rss/foot-ligue-1-c12.xml';
        this.detail = null;


        this.getInfos = function () {

            return RssService.load(rssInfoUrl);
        }
        this.getFreshLeague = function(){

            return RssService.load(rssFrenshUrl);
        };
        this.getSpanLeague = function(){

            return RssService.load(rssSpanUrl);
        }
        this.setInfo = function (info) {
            this.detail = info;
        };
    };
    InfoService.$inject = ["$location", 'RssService'];
    angular.module("LEAGUE").service("InfoService", InfoService)


})();
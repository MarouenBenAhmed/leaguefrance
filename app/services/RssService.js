/**
 * Created by MBM info on 06/04/2015.
 */
/**
 * Created by MBM info on 05/04/2015.
 */
(function () {
    var RssService = function ($http) {

        this.load = function (src) {

            return $.ajax({url:'http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&callback=?&q='+src,
                dataType: 'jsonp',type:'GET'});

        }
    };
    RssService.$inject = ['$http'];
    angular.module("LEAGUE").service("RssService", RssService)


})();